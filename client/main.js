import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.uploadForm.onCreated(function () {
  this.currentUpload = new ReactiveVar(false);
});

Template.b.helpers({
  message: function () {
    let val = Message.findOne();
    if(val){
      return val.text;
    }
    return `--- User context id:
--- User x-mtok id:
--- Client param x-mtok:
--- Server sessions:`;
  }
});

Template.uploadForm.helpers({
  currentUpload: function () {
    return Template.instance().currentUpload.get();
  }
});

Template.uploadForm.events({
  'change #fileInput': function (e, template) {
    if (e.currentTarget.files && e.currentTarget.files[0]) {
      // We upload only one file, in case
      // multiple files were selected
      var upload = Images.insert({
        file: e.currentTarget.files[0],
        streams: 'dynamic',
        chunkSize: 'dynamic'
      }, false);

      upload.on('start', function () {
        template.currentUpload.set(this);
      });

      upload.on('end', function (error, fileObj) {
        if (error) {
          alert('Error during upload: ' + error);
        } else {
          alert('File "' + fileObj.name + '" successfully uploaded');
        }
        template.currentUpload.set(false);
      });

      upload.start();
    }
  }
});

Template.file.onRendered(function () {
//  console.log(this);
});

Template.file.helpers({
  sess: function () {
    return (Meteor.connection && Meteor.connection._lastSessionId) ? Meteor.connection._lastSessionId : null;
  },
  urlRedir:function(){
    let id = this.file._id;
    return `/transactionFileDownload/${id}`;
//    transactionFileDownload/:token
  }
});

Template.files.helpers({
  files: function () {
    return Images.find().each();
  }
});


  Router.route('/', function () {
    this.render('b');
  });
  
  Router.configure({
    layoutTemplate: 'IronBody',
  });