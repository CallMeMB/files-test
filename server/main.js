import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  
  if (Meteor.users.find().count() === 0) {
    console.log('Creating first user.');
    var id = Accounts.createUser({
      email: "admin@constellationsoft.com",
      password: "admin"
    });

    Meteor.users.update({_id: id},
            {$set: {
                role: 'admin'
              }});
  }else{
  }
  
});


Router.route("/transactionFileDownload/:token", {
  where: "server"
}).get(function () {


  let item = Images.findOne({_id: this.params.token});
  var request = this.request;
  let response = this.response;
  let session = request.Cookies.cookies.x_mtok;

  if (!item || !session) {
    console.log('blocked');
    return;
  }
  let url = `${item.link()}?play=true&xmtok=${session}`;
  console.log(url);
  
  response.setHeader('Cookie', request.Cookies);
  
  response.writeHead(302, {
    'Location': url
  });
  response.end();

//  console.log(' -- File request',request.Cookies.cookies.x_mtok);

//  this.response.end(JSON.stringify({ok:true}));

});